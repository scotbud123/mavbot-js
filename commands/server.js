module.exports = {
	name: 'server',
	aliases: ['serverinfo', 'info'],
	cooldown: 5,
	description: 'Display info about this server.',
	args: false,
	guildOnly: true,
	execute(message) {
		message.channel.send(`Server name: ${message.guild.name}\nTotal members: ${message.guild.memberCount}`);
	},
};