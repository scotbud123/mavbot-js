const XMLHttpRequest = require('xmlhttprequest').XMLHttpRequest;

module.exports = {
	name: 'minetrack',
	cooldown: 5,
	description: 'Command that reports when players log onto the MC server.',
	guildOnly: false,
	args: false,
	execute(message, args) {
        //create new XMLHttpRequest
        const xmlHttp = new XMLHttpRequest();

        xmlHttp.onreadystatechange = () => {
            //check that response is complete
            if(xmlHttp.readyState === 4) {
                //create obj from response
                console.log(`\nCONSOLE LOG: ${xmlHttp.responseText}\n`);
                const resp = JSON.parse(xmlHttp.responseText);
                //check that hostname exists
                if(!resp.hostname) {
                    message.channel.send(`Can't reach server!`);
                    return;
                }
                //create answer message
                //let response = resp.hostname;
                let response = '';
                if(resp.online) {
                    response += 'Server is online. Online players: ';
                    if(resp.players.online) {
                        response += resp.players.online;
                        response += `\nList of players:\n${resp.players.list}`;
                    }
                    else {
                        response += 'none';
                    }
                }
                else {
                    response += 'Server is offline!'
                }
                //send answer
                message.channel.send(response);
            }
        }
        //open and send xhr
        xmlHttp.open('GET', 'https://api.mcsrvstat.us/2/christomavrick.asuscomm.com');
        xmlHttp.send();
	},
};