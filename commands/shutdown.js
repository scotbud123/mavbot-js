const { OWNER_USER_ID } = require('../config.json');

module.exports = {
	name: 'shutdown',
	description: 'Shuts the bot down (only usable by bot owner)',
	execute(message, args, client) {
		if (message.author.id === `${OWNER_USER_ID}`) {
            message.channel.send("Bye! :wave:");
            console.log("Shutting down...");
            process.exit();
        }
        else {
            message.channel.send('Sorry, you don\'t have permission to use this command!');
        }
	},
};