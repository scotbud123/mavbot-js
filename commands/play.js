const ytdl = require('ytdl-core-discord');

module.exports = {
	name: 'play',
	cooldown: 5,
	description: 'Play music from YouTube!',
	guildOnly: true,
    args: true,
    aliases: ['p', 'pl'],
	async execute(message, args) {
        if (args.length !== 1) {
			return message.reply("You must send 1 argument, and only 1!");
        }

        let expression = "^(https?\:\/\/)?(www\.youtube\.com|youtu\.?be)\/.+$";
        let regex = new RegExp(expression);
        let url = args[0];

        if (!url.match(regex)) {
            return message.reply("You must pass a YouTube URL!");
        }
        
        if (message.member.voice.channel) {
            const connection = await message.member.voice.channel.join();

            const dispatcher = connection.play(await ytdl(url), { type: 'opus' });

            dispatcher.on('start', () => {
                console.log(`Now playing: ${url}! Requested by: ${message.member.user.tag}`);
                myMessage = message.reply(` Now playing: <${url}>`);
            });

            dispatcher.on('finish', () => {
                console.log(`${url} has finished playing!\n------------\n`);
                message.reply(` <${url}> has finished playing!`);
            });

            dispatcher.on('error', console.error);
        }
        else {
            message.reply("You're not in a voice channel!");
        }
	},
};