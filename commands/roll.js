module.exports = {
	name: 'roll',
	cooldown: 0.1,
	description: 'Rolls a dice in NdN format',
	guildOnly: false,
	args: true,
	execute(message, args) {
        if (args.length !== 1) {
			return message.channel.send("You must send 1 argument, in a NdN format.");
		}

		const regex = "([1-9]\\d*)?d([1-9]\\d*)([/x][1-9]\\d*)?([+-]\\d+)?";

		if (args[0].match(regex)) {
			let diceArray = args[0].split("d");

			let amountOfDice = Number(diceArray[0]);
			let sidesOfDice = Number(diceArray[1]);

			let rolls = [];
			let total = 0;

			for (let i = 0; i < amountOfDice; i++) {
				let roll = Math.floor(Math.random() * sidesOfDice) + 1;

				total = total + roll;
				
				rolls.push(roll);
			}

			message.channel.send(`${rolls}\n Total = ${total}`);
		}
		else {
			message.channel.send("That's not NdN format!");
		}
	},
};