const fetch = require('node-fetch');

module.exports = {
	name: 'mavtts',
	cooldown: 5,
	description: 'Use Streamelements API to make TTS calls and play them over Discord',
	guildOnly: true,
    args: false,
    aliases: ['t', 'mytts'],
	async execute(message, args) {
        if (args.length !== 1) {
			return message.reply("You must send 1 argument, and only 1!");
        }
        
        if (message.member.voice.channel) {
            let text = args[0];
            let speak = await fetch("https://api.streamelements.com/kappa/v2/speech?voice=Brian&text=" + encodeURIComponent(text.trim()));

            if (speak.status != 200) { //If the return status is anything but OK
                console.log(await speak.text());
                return;
            }

            console.log(`\nspeak is: ${speak}\n`);
            
            let mp3 = await speak.blob();

            console.log(`\nmp3 is: ${mp3}\n`);

            //let blobUrl = URL.createObjectURL(mp3);

            //console.log(`\nblobUrl is: ${blobUrl}\n`);

            const connection = await message.member.voice.channel.join();

            // Create a dispatcher
            const dispatcher = connection.play(mp3);

            dispatcher.on('start', () => {
                console.log(`Now playing: TTS! Requested by: ${message.member.user.tag}`);
            });

            dispatcher.on('finish', () => {
                console.log(`TTS has finished playing!\n------------\n`);
                connection.disconnect();
                dispatcher.destroy();
            });

            dispatcher.on('error', console.error);
        }
        else {
            message.channel.send("You're not in a voice channel!");
        }
	},
};