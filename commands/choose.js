module.exports = {
	name: 'choose',
	cooldown: 0.5,
	description: 'Chooses between multiple choices.',
	guildOnly: false,
	args: true,
	execute(message, args) {
        if (args.length < 2) {
            message.channel.send(`You must send at least 2 choices, ${message.author}!`);
        }
        else {
			message.channel.send(args[Math.floor(Math.random() * args.length)]);
        }
	},
};