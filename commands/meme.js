const { prefix } = require('../config.json');

module.exports = {
	name: 'meme',
	cooldown: 5,
	description: 'Play a meme sound file!',
	guildOnly: true,
    args: true,
    aliases: ['m', 'memes'],
    usage: `jeff | hello | mana | citizens | ree | tendies | drugs | jedi | wed`,
	async execute(message, args) {
        if (args.length !== 1) {
			return message.channel.send("You must send 1 argument, and only 1!");
        }
        
        // Join the same voice channel of the author of the message
	    if (message.member.voice.channel) {
            let fileName = "";
            
            //Switch on passed arg for file name string
            switch (args[0].toLowerCase()) {
                case "jeff":
                    fileName = "name_jeff";
                    break;
                case "hello":
                    fileName = "hello_there";
                    break;
                case "mana":
                    fileName = "mana";
                    break;
                case "citizens":
                    fileName = "citizens";
                    break;
                case "ree":
                    fileName = "ree";
                    break;
                case "tendies":
                    fileName = "tendies";
                    break;
                case "drugs":
                    fileName = "drugs";
                    break;
                case "jedi":
                    fileName = "jedi";
                    break;
                case "wed":
                    let myDate = new Date();
                    let today = myDate.getDay();

                    if (today === 3) { //3 for Wednesday
                        fileName = "my_dudes";
                    }
                    else {
                        return message.channel.send("It's not Wednesday, my dudes!");
                    }
                    break;
                default:
                    return message.channel.send(`No meme with that name found! Try ${prefix}help meme for more info.`);
            }
            
            const connection = await message.member.voice.channel.join();

            // Create a dispatcher
            const dispatcher = connection.play(`./resources/${fileName}.mp3`);

            dispatcher.on('start', () => {
                console.log(`Now playing: ${fileName}! Requested by: ${message.member.user.tag}`);
            });

            dispatcher.on('finish', () => {
                console.log(`${fileName} has finished playing!\n------------\n`);
                connection.disconnect();
                dispatcher.destroy();
            });

            dispatcher.on('error', console.error);
        }
        else {
            message.channel.send("You're not in a voice channel!");
        }
	},
};