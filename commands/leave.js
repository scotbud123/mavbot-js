module.exports = {
	name: 'leave',
	cooldown: 5,
	description: "Forces the bot to leave if it's currently in a voice channel.",
	guildOnly: true,
    args: false,
    aliases: ['l', 'lv'],
	async execute(message, args) {
        if (message.guild.me.voice.channel) {
            const connection = await message.member.voice.channel.join();
            
            connection.disconnect();
            //dispatcher.destroy();
        }
        else {
            message.reply("I AM NOT IN VOICE");
        }
	},
};